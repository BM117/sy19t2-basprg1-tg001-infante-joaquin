#include <iostream>

using namespace std;

int HPchecker(float HP, float fullHP) {
	int HPpercentage;

	HPpercentage = (HP / fullHP) * 100;

	if (HPpercentage == 100) {
		return 0;
	}
	else if (HPpercentage >= 50) {
		return 1;
	}
	else if (HPpercentage >= 20) {
		return 2;
	}
	else if (HPpercentage >= 1) {
		return 3;
	}
	else {
		return 4;
	}
}

int main() {
	switch (HPchecker(10, 100)) {
	case 0:
		cout << "Full HP";
		break;
	case 1:
		cout << "Green";
		break;
	case 2:
		cout << "Yellow";
		break;
	case 3:
		cout << "Red";
		break;
	case 4:
		cout << "Pepsi";
	}

	system("pause");
		return 0;

}
