#include <iostream>

using namespace std;

int main()
{

	int v, n;
	int a = 0;

	cout << "Starting Numerical Value: ";
	cin >> v;
	cout << "Number of Nth terms; ";
	cin >> n;

	for (int i = v; i <= n; i++)
	{
		a = a + i;
	}

	cout << "Sum of Arithmetic Progression; " << a << endl;

	system("pause");
	return 0;
}
