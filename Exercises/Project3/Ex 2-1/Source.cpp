#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Initialize variables for input
	int x;
	int y;

	// Get input
	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;

	bool check1 = x == y;
	bool check2 = x != y;
	bool check3 = x > y;
	bool check4 = x < y;
	bool check5 = x >= y;
	bool check6 = x <= y;


	cout << x << " == " << y << " = " << check1 << endl;
	cout << x << " != " << y << " = " << check2 << endl;
	cout << x << " > " << y << " = " << check3 << endl;
	cout << x << " < " << y << " = " << check4 << endl;
	cout << x << " >= " << y << " = " << check5 << endl;
	cout << x << " <= " << y << " = " << check6 << endl;



	system("pause");
	return 0;

}