#include <iostream>

using namespace std;

int main()
{
	int active = 1;

	int n;
	cout << "Input How tall the tree is: ";
	cin >> n;

	while (active == 1)
	{
		cout << " * " << endl;
		cout << " ** " << endl;
		cout << " *** " << endl;
		cout << " **** " << endl;
		cout << " ***** " << endl;

		if (n == 5)
		{
			active = 0;
		}
	}


	system("pause");
	return 0;
}