#include <iostream>
#include <ctime>

using namespace std;


int main() 
{
	srand((unsigned)time(0));

	int active = 1;

	while (active == 1)
	{
		int dice1;
		dice1 = rand() % 6 + 1;

		int dice2;
		dice2 = rand() % 6 + 1;

		cout << "Dice 1 rolled: " << dice1 << endl;
		cout << "Dice 2 rolled: " << dice2 << endl;

		int sumDice;
		sumDice = dice1 + dice2;

		if (sumDice % 2 == 0)
		{
			active = 0;
		}
	}
	
	system("pause");
	return 0;
}
