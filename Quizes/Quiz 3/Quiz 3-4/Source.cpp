#include <iostream>

using namespace std;

int main()
{

	int fib;
	cout << "Input amount of fibonacci terms: ";
	cin >> fib;

	for (int i = 0; i < fib; i++)
		cout << (1 + i) + 2 << " , ";

	system("pause");
	return 0;
}
