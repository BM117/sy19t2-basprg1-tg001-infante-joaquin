#include <iostream>

using namespace std;

int main()
{
	// Initialize variables for input
	float x;
	float y;
	float z;
	float a;
	float b;
	float c;

	// Get input
	cout << "Input first number: ";
	cin >> x;
	cout << "Input second number: ";
	cin >> y;
	cout << "Input third number: ";
	cin >> z;
	cout << "Input first number: ";
	cin >> a;
	cout << "Input fifth number: ";
	cin >> b;
	cout << "Input total amount of numbers: ";
	cin >> c;

	// Compute
	float sum = x + y;
	float add = sum + z;
	float addition = add + a;
	float plus = addition + b;
	float quot = plus / c;

	// Output result
	cout << x << " + " << y << " = " << sum << endl;
	cout << sum << " + " << z << " = " << add << endl;
	cout << add << " + " << a << " = " << addition << endl;
	cout << addition << " + " << b << " = " << plus << endl;
	cout << plus << " / " << c << " = " << quot << endl;


	system("pause");
	return 0;
}

