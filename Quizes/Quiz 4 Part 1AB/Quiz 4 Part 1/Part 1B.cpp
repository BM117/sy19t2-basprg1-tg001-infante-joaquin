#include <iostream>

using namespace std;

int findMissingNo(int arr[], int len) {
	int temp;

	temp = ((len + 1)*(len + 2)) / 2;

	for (int i = 0; i < len; i++)
		temp -= arr[i];

	return temp;
}

int main() {

	int n;
	cout << "Enter the size of array: ";
	cin >> n;    
	int arr[1];
	cout << "Enter array elements: ";
	for (int i = 0; i < n; i++) {
		cin >> arr[i];
	}
	int missingNo = findMissingNo(arr, 100);
	cout << "The missing number " << missingNo;

	return 0;
}
